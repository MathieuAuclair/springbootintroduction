package co.ligneauto.webapplication.models;

public class Post {
    private String title;
    private String body;
    private int id;
    private int userId;

    public Post() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Post(String title, String body, int id, int userId) {
        this.setTitle(title);
        this.setBody(body);
        this.setId(id);
        this.setUserId(userId);
    }
}
