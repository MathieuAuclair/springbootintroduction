package co.ligneauto.webapplication.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.client.WebClient;

import co.ligneauto.webapplication.models.Post;
import reactor.core.publisher.Flux;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String Index() {
		return "Index";
	}

	@RequestMapping("/reactive")
	public @ResponseBody Flux<Post> requestPublicJsonApi()
	{
		String url = "https://jsonplaceholder.typicode.com";
		String endpoint = "/posts";

		WebClient client = WebClient.builder()
			.baseUrl(url)
			.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.build();

		return client.get()
			.uri(endpoint)
			.retrieve()
			.bodyToFlux(Post.class);
	}
}
