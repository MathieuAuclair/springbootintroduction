package co.ligneauto.webapplication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import co.ligneauto.webapplication.dataaccesslayer.CustomerRepository;
import co.ligneauto.webapplication.models.Customer;

@Controller 
@RequestMapping(path = "/demo")
public class CustomerController {
    @Autowired 
    private CustomerRepository customerRepository;

    @PostMapping(path = "/add") 
    public @ResponseBody String addNewUser(@RequestParam String firstName, @RequestParam String lastName) {
        Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerRepository.save(customer);
        return "Saved";
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Customer> getAllUsers() {
        return customerRepository.findAll();
    }
}