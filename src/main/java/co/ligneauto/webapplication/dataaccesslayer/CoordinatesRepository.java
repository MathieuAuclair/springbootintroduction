package co.ligneauto.webapplication.dataaccesslayer;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.ligneauto.webapplication.models.TravelCoordinates;

@Repository
public interface CoordinatesRepository extends CrudRepository<TravelCoordinates, Long> {
    List<TravelCoordinates> findByLatitude(String latitude);
    List<TravelCoordinates> findByLongitude(String longitude);
    TravelCoordinates findById(long id);
}
