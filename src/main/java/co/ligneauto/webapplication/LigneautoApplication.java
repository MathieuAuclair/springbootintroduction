package co.ligneauto.webapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import co.ligneauto.webapplication.dataaccesslayer.CoordinatesRepository;
import co.ligneauto.webapplication.dataaccesslayer.CustomerRepository;
import co.ligneauto.webapplication.dataaccesslayer.TravelRepository;
import co.ligneauto.webapplication.models.Customer;
import co.ligneauto.webapplication.models.TravelCoordinates;
import co.ligneauto.webapplication.models.Travel;

import java.util.HashSet;

@SpringBootApplication
public class LigneautoApplication {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CoordinatesRepository coordinatesRepository;

	@Autowired
	private TravelRepository travelRepository;

	public static void main(String[] args) {
		SpringApplication.run(LigneautoApplication.class, args);
	}

	@Bean
	public CommandLineRunner SeedCustomers() {
		return (args) -> {
			// save a few customers
			customerRepository.save(new Customer("Jack", "Bauer"));
			customerRepository.save(new Customer("Chloe", "O'Brian"));
			customerRepository.save(new Customer("Kim", "Bauer"));
			customerRepository.save(new Customer("David", "Palmer"));
			customerRepository.save(new Customer("Michelle", "Dessler"));
			coordinatesRepository.save(new TravelCoordinates("45.2665", "72.1480"));
			coordinatesRepository.save(new TravelCoordinates("45.5750", "72.5261"));

			Iterable<TravelCoordinates> coordinates = coordinatesRepository.findAll();

			HashSet<TravelCoordinates> coordinatesSet = new HashSet<TravelCoordinates>();

			for (TravelCoordinates item : coordinates) {
				coordinatesSet.add(item);
			}

			Travel travel = new Travel("Roxton Falls -> Magog", coordinatesSet);

			travelRepository.save(travel);
		};
	}
}