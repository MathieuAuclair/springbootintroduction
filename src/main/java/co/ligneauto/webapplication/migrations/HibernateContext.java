package co.ligneauto.webapplication.migrations;

import java.util.EnumSet;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

public class HibernateContext {
    private static StandardServiceRegistry registry;
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) {
            return sessionFactory;
        }

        try {
            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder().configure(); 
            registry = registryBuilder.build();
            MetadataSources sources = new MetadataSources(registry);
            MetadataBuilder metadataBuider = sources.getMetadataBuilder();
            Metadata metadata = metadataBuider.build();
            sessionFactory = metadata.getSessionFactoryBuilder().build();

            SchemaExport schemaExport = new SchemaExport();
            schemaExport.setFormat(true);
            schemaExport.setOutputFile("create.sql");
            schemaExport.createOnly(EnumSet.of(TargetType.SCRIPT), metadata);
        } catch (Exception exception) {
            exception.printStackTrace();
            if (registry != null) {
                StandardServiceRegistryBuilder.destroy(registry);
            }
        }
    
        return sessionFactory;
    }

    public static void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }
}
