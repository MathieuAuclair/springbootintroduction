<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:Layout>
    <jsp:attribute name="header">
        <div class="text-center">
            <div class="text-white pt-5 mt-5 w-75 m-auto">
                <h1>Ever wanted to save money while saving the planet?</h1>
                <p>transform your daily car travel into a bus line to limit the amount of cars on the road</p>
            </div>
            <button class="btn btn-primary">Get Started!</button>
        </div>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <section class="bg-dark p-5 mt-5">
            <p class="text-white text-center">Mathieu Auclair 2021 &copy;</p>
        </section>
    </jsp:attribute>
    <jsp:body>
        <section class="bg-dark p-5">
            <h1 class="text-center text-white">Why does it matter?</h1>
            <div class="row pt-5">
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row pb-5 mt-2">
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-5 card">
            <h1 class="text-center p-3">See all available travels!</h1>
            <iframe
                src='https://api.mapbox.com/styles/v1/mapbox/streets-v11.html?title=true&zoomwheel=false&access_token=pk.eyJ1IjoibWF0aGlldWF1Y2xhaXIiLCJhIjoiY2trenFsNDh3Mjd1aTJ3cW94YjFham51eiJ9.-wzt0jV_u3Va5slfZfixOg#15/37.771/-122.436' 
                width='100%' 
                height='650px'>
            </iframe>  
        </section>
    </jsp:body>
</t:Layout>