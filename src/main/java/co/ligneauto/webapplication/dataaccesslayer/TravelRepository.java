package co.ligneauto.webapplication.dataaccesslayer;
import org.springframework.data.repository.CrudRepository;

import co.ligneauto.webapplication.models.Travel;

public interface TravelRepository extends CrudRepository<Travel, Long> {
    Travel findById(long id);
}
