package co.ligneauto.webapplication.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Travel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany
    private Set<TravelCoordinates> coordinates;

    public Travel() {
    }

    public Travel(String name, Set<TravelCoordinates> coordinates) {
        this.name = name;
        this.coordinates = coordinates;
    }

    private long getId()
    {
        return id;
    }

    private String getName()
    {
        return name;
    }

    private void setId(long id)
    {
        this.id = id;
    }

    private void setName(String name)
    {
        this.name = name;
    }
}
